package fishcrabprawn;

import org.junit.Test;

import static org.junit.Assert.*;

public class PunterTest {

    @Test
    public void loseBet() {
        //setup
        Punter punter = new Punter("Alfred", 100);
        punter.placeBet(10);

        //when
        punter.loseBet();

        //then
        assertEquals(90, punter.getBalance());
    }

    @Test
    public void placeBet_betExceedLimit() {
        //setup
        int bet = 90;
        int limit = 10;
        int balance = 100;
        Punter punter = new Punter("Alfred", balance, limit);

        //when
        punter.placeBet(bet);

        //then
        assertEquals(limit, punter.getBalance());
    }
}