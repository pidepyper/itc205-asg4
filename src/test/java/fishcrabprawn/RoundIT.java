package fishcrabprawn;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class RoundIT {
    //setup
    Round round = new Round();

    @Test
    public void play_winningRoundForOneFace() {
        //setup
        Punter punter = new Punter("Alfred", 100);
        Die die1 = mock(Die.class);
        Die die2 = mock(Die.class);
        Die die3 = mock(Die.class);
        when(die1.getFace()).thenReturn(Face.CRAB);
        when(die2.getFace()).thenReturn(Face.FISH);
        when(die3.getFace()).thenReturn(Face.FISH);
        List<Die> dice = new ArrayList<>();
        dice.add(die1);
        dice.add(die2);
        dice.add(die3);

        //when
        round.play(punter, dice, Face.CRAB, 10);

        //then
        assertEquals(110, punter.getBalance());
    }

    @Test
    public void play_checkOdds() {
        //setup
        Punter punter = mock(Punter.class);
        when(punter.getBalance()).thenReturn(100);
        when(punter.getLimit()).thenReturn(10);
        List<Die> dice = new ArrayList<>();
        dice.add(new Die());
        dice.add(new Die());
        dice.add(new Die());
        int timesWon = 0;
        int gamesPlayed = 10000;

        //when
        for(int i = 0 ; i < gamesPlayed ; i++) {
            int winnings = round.play(punter, dice, Face.CRAB, 1);
            if (winnings > 0) timesWon++;
        }

        //then
        double winRatio = timesWon / gamesPlayed;
        System.out.println(timesWon);
        assertEquals(0.42, winRatio, 0.01);
    }
}
