import os
import subprocess
import sys


def main(args):
    mapping = {
        'clean': clean,
        'build': build,
        'run': run
    }
    for arg in args:
        if arg in mapping:
            mapping[arg]()
    return


def clean():
    extensions = ['.class', '.obj']
    for filename in os.listdir('.'):
        for extension in extensions:
            if filename.endswith(extension):
                os.unlink(filename)
    return


def build():
    p = subprocess.call(['javac.exe', 'Main.java'])
    return


def run():
    p = subprocess.Popen(['java.exe', 'Main'])
    p.communicate()
    return


main(sys.argv)
